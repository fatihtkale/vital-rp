# Vital RP
Repo for vital rp

---

# Setting it up

start by going into server and web and running the follow cmd

```js
npm install
```

---

# Database Setup
After that install <a href="https://nosqlbooster.com/">NoSqlBooster</a>
&
Install <a href="https://www.mongodb.com/try/download/community">MongoDB</a>

now head into
```js
C:\Program Files\MongoDB\Server\4.4\bin
```
or where ever you have it installed and type.
```js
mongod.exe
```
make sure you have the cmd open.
Now open up NoSqlBooster and press connect on the top left
should look like this
![alt text](https://cdn.discordapp.com/attachments/732270450486542536/745599368312127539/unknown.png)

Now press the "Save & Connect"

after you have made a db (make sure its called vitalrp)

![alt text](https://cdn.discordapp.com/attachments/732270450486542536/745597805762183282/unknown.png)

press import and select all the databasefiles and import them in and you're good to go!!!

---